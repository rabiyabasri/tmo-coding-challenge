import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from "@ngrx/effects";
import { map, switchMap, catchError } from "rxjs/operators";
import { of, from } from "rxjs";

import { StocksService } from "../../services/stocks.service";
import * as DataActions from "../actions/data.actions";

import { QueryResponse } from '../../models/stocks';

@Injectable({
  providedIn:'root'
})
export class DataEffects {
  constructor(private actions: Actions, private dataService: StocksService) {}

  @Effect()
  getStocks$ = this.actions.pipe(
    ofType(DataActions.SEARCH),
    map(action => (action as any).payload),
    switchMap((queryData) => {
      return this.dataService.getStocks(queryData).pipe(
        map(resp => new DataActions.SearchDone(resp as QueryResponse[])),
        catchError(error =>
          of(new DataActions.LoadDataFailure({ error: error }))
        )
      );
    })
  );
}