import * as fromData from "../actions/data.actions";

import { QueryResponse } from '../../models/stocks';

export interface DataState {
  query: string;
  loading: boolean;
  error: any;
  results: QueryResponse[];
}

export const initialState: DataState = {
  query: '',
  loading: false,
  error: null,
  results: [],
};

export function reducer(
  state = initialState,
  action: fromData.ActionsUnion
): DataState {
  switch (action.type) {
    case fromData.SEARCH: {
      return {
          ...state,
          loading: false,
          query: action.payload
      }
  }
    case fromData.SEARCH_DONE: {
        return {
            ...state,
            loading: false,
            results: action.payload
        }
    }

    case fromData.SEARCH_FAILURE: {
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    }

    default: {
      return state;
    }
  }
}

export const getItems = (state: DataState) => state.results;
export const getSymbol = (state: DataState) => state.query;