import { Action } from "@ngrx/store";
import { QueryResponse } from '../../models/stocks';

export const SEARCH = 'Enter Symbol';
export const SEARCH_DONE = 'Stock Data Done';
export const SEARCH_FAILURE = 'Stock Data Failure';


export class Search implements Action {
  readonly type = SEARCH;
  constructor(public payload: string) { };
}

export class SearchDone implements Action {
  readonly type = SEARCH_DONE;
  constructor(public payload: QueryResponse[]) { };
}

export class LoadDataFailure implements Action {
  readonly type = SEARCH_FAILURE;

  constructor(public payload: { error: any }) {}
}

export type ActionsUnion = Search | SearchDone | LoadDataFailure;