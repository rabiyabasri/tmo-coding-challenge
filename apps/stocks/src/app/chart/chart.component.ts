import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { StocksService } from '../services/stocks.service';
import { Period } from '../models/stocks';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromRoot from '../state/reducers';

@Component({   
  selector: 'coding-challenge-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})   
export class ChartComponent implements OnInit, OnDestroy {
  @Input() data$: Observable<any>;
  chartData: any;
  symbol: string;
  displayPeriods: boolean = false;
  periodSubscription: Subscription;
  stockSubscription: Subscription;
  querySubscription: Subscription;
  timePeriods: Period[] = [
    { viewValue: 'All available data', value: 'max' },
    { viewValue: 'Five years', value: '5y' },
    { viewValue: 'Two years', value: '2y' },
    { viewValue: 'One year', value: '1y' },
    { viewValue: 'Year-to-date', value: 'ytd' },
    { viewValue: 'Six months', value: '6m' },
    { viewValue: 'Three months', value: '3m' },
    { viewValue: 'One month', value: '1m' }
  ];
  chart: {
    title: string;
    type: string;
    data: any;
    columnNames: string[];
    options: any;
  };
  getQuery$ = this.stocksService.querySelectedQuery$;
  constructor(private stocksService: StocksService, private store: Store<fromRoot.AppState>){ }
  
  ngOnInit() {
    this.chart = {
      title: '',     
      type: 'LineChart',
      data: [],
      columnNames: ['period', 'close'],
      options: { title: `Stock price`, width: '800', height: '400' }
    };
    this.stockSubscription = this.data$.subscribe(newData => {
      this.chartData = newData;
      this.displayPeriods = true;
    });
    this.getQuery$.subscribe(queryData => {
      let symbol = Object.values(queryData);
      this.symbol = symbol[0];
    });
  }
  periodEvent(value: string){
    let data = { symbol: this.symbol, period: value};
    this.periodSubscription = this.stocksService.getStocks(data).subscribe(data => {
      this.chartData = [];
      data.forEach(element => {   
            this.chartData.push([new Date(element.date), element.close]);
          });
    });
  }
  ngOnDestroy() {
    if(this.stockSubscription){
      this.stockSubscription.unsubscribe();
    }
    if(this.periodSubscription){
      this.periodSubscription.unsubscribe();
    }
    if(this.querySubscription){
      this.querySubscription.unsubscribe();
    }
  }
}
