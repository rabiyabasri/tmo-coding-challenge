import { inject, async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartComponent } from './chart.component';
import { StocksComponent } from './../stocks/stocks.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyMaterialModule } from  '../material.module';
import { ReactiveFormsModule }    from '@angular/forms';
import { GoogleChartsModule } from 'angular-google-charts';
import { StocksService } from '../services/stocks.service';
import {mockStore} from '../mockStore';
import { Subject, Observable, of} from 'rxjs';
import { StoreModule, Store, Action, select } from '@ngrx/store';
import { reducers, metaReducers } from '../state/reducers';
import * as fromData from '../state/reducers/data.reducer';

describe('ChartComponent', () => {
  let component: ChartComponent;
  let fixture: ComponentFixture<ChartComponent>;
  
  const actions = new Subject<Action>();
  const states = new Subject<fromData.DataState>();
  const store = mockStore<fromData.DataState>({ actions, states });
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartComponent, StocksComponent ],
      imports:[
        HttpClientModule,
        BrowserAnimationsModule,
        MyMaterialModule,
        ReactiveFormsModule,
        GoogleChartsModule,
        StoreModule.forRoot(reducers, { metaReducers })
      ],
      providers: [
        StocksService, {provide: Store, useValue: store}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('Intial ngOnInit',() =>{
    spyOn(component,"ngOnInit");
    component.ngOnInit();
    expect(component.ngOnInit).toHaveBeenCalled();
  });
});
