export interface Period {
    value: string;
    viewValue: string;
}
export interface QueryResponse {
    date: string;
    open: number;
    close: number;
    high: number;
    low: number;
    volume: number;
    uOpen: number;
    uClose: number;
    uHigh: number;
    uLow: number;
    uVolume: number;
    change: number;
    changePercent: number;
    label: string;
    changeOverTime: number;
};