import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { StocksComponent } from './stocks/stocks.component';
import { ChartComponent } from './chart/chart.component';

import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyMaterialModule } from  './material.module';
import { ReactiveFormsModule }    from '@angular/forms';
import { GoogleChartsModule } from 'angular-google-charts';
import { StocksService } from './services/stocks.service';

import { StoreModule, Action } from '@ngrx/store';
import { reducers, metaReducers } from './state/reducers';
import { EffectsModule } from '@ngrx/effects';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        StocksComponent,
        ChartComponent
      ],
      imports:[
        HttpClientModule,
        BrowserAnimationsModule,
        MyMaterialModule,
        ReactiveFormsModule,
        GoogleChartsModule,
    EffectsModule.forRoot([]),
    StoreModule.forRoot(reducers, { metaReducers })
      ],
      providers: [
        StocksService
      ]
    }).compileComponents();
  }));    
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));  
});
