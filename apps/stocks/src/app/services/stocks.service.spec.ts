import { TestBed, inject } from '@angular/core/testing';

import { StocksService } from './stocks.service';
import {mockStore} from '../mockStore';
import { Subject, Observable, of} from 'rxjs';
import { StoreModule, Store, Action, select } from '@ngrx/store';
import { reducers, metaReducers } from '../state/reducers';
import * as fromData from '../state/reducers/data.reducer';

import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { environment } from '../../environments/environment';

describe('StocksService', () => {
  let httpMock: HttpTestingController;
  const actions = new Subject<Action>();
  const states = new Subject<fromData.DataState>();
  const store = mockStore<fromData.DataState>({ actions, states });
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, StoreModule.forRoot(reducers, { metaReducers })],
      providers: [StocksService, {provide: Store, useValue: store}]
    });
  });
  beforeEach(() => {
    httpMock = TestBed.get(HttpTestingController);
  });
  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpMock.verify();
  });
  it('should be created', inject([StocksService], (service: StocksService) => {
    expect(service).toBeTruthy();
  }));
});
