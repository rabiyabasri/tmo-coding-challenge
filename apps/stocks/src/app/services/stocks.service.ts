import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { QueryResponse } from '../models/stocks';
import { Store, select } from "@ngrx/store";
import { map, skip } from 'rxjs/operators';

import { AppState, getAllItems, getSymbolState } from "../state/reducers";

@Injectable({
  providedIn: 'root'
})
export class StocksService {

  constructor(private http: HttpClient, private store: Store<AppState>) { }
  getStocks(queryData: any): Observable<any> {  
  return this.http.get<QueryResponse[]>(environment.apiURL + `/stable/stock/${queryData.symbol}/chart/${queryData.period}?token=${environment.apiKey}`);  
  }    
  priceQueries$ = this.store.pipe(
    select(getAllItems),
    skip(1),
    map(priceQueries =>
      priceQueries.map(priceQuery => [new Date(priceQuery.date), priceQuery.close])
    )
  );
  querySelectedQuery$ = this.store.pipe(
  select(getSymbolState),
  skip(1),
  map(priceQueries => priceQueries));
}
