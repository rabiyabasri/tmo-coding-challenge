import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StocksComponent } from './stocks.component';
import { ChartComponent } from './../chart/chart.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MyMaterialModule } from  '../material.module';
import { ReactiveFormsModule }    from '@angular/forms';
import { GoogleChartsModule } from 'angular-google-charts';
import { StocksService } from '../services/stocks.service';

import {mockStore} from '../mockStore';
import { Subject, Observable, ReplaySubject} from 'rxjs';
import { StoreModule, Store, Action, select } from '@ngrx/store';
import { reducers, metaReducers } from '../state/reducers';
import * as fromData from '../state/reducers/data.reducer';

describe('StocksComponent', () => {
  let component: StocksComponent;
  let fixture: ComponentFixture<StocksComponent>;
  const actions = new Subject<Action>();
  const states = new Subject<fromData.DataState>();
  const store = mockStore<fromData.DataState>({ actions, states });
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StocksComponent, ChartComponent ],
      imports:[
        HttpClientModule,
        BrowserAnimationsModule,
        MyMaterialModule,
        ReactiveFormsModule,
        GoogleChartsModule,
        StoreModule.forRoot(reducers, { metaReducers })
      ],
      providers: [
        StocksService, {provide: Store, useValue: store}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
});
