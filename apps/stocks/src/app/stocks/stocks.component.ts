import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StocksService } from '../services/stocks.service';
import { Period } from '../models/stocks';
import { Store } from '@ngrx/store';
import * as fromRoot from '../state/reducers';
import * as Actions from '../state/actions/data.actions';
@Component({   
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.css']
})   
export class StocksComponent implements OnInit {
  stockForm: FormGroup;
  submitted: boolean = false;
  timePeriods: Period[] = [
    { viewValue: 'All available data', value: 'max' },
    { viewValue: 'Five years', value: '5y' },
    { viewValue: 'Two years', value: '2y' },
    { viewValue: 'One year', value: '1y' },
    { viewValue: 'Year-to-date', value: 'ytd' },
    { viewValue: 'Six months', value: '6m' },
    { viewValue: 'Three months', value: '3m' },
    { viewValue: 'One month', value: '1m' }
  ];
  quotes$ = this.stocksService.priceQueries$;

  constructor(private formBuilder: FormBuilder,
  private stocksService: StocksService,private store: Store<fromRoot.AppState>) { }
    
  ngOnInit() {
    this.stockForm = this.formBuilder.group({
      symbol: ['', Validators.required],
      period: [null, Validators.required]
    });
  }
  get f() { return this.stockForm.controls; }
  fetchQuote(){
    this.submitted = true;
    if (this.stockForm.valid) {
        this.store.dispatch(new Actions.Search(this.stockForm.value));
    }
  }
}
